import { LOGIN_ERROR, LOGIN_SUCCESS, SIGNOUT_SUCCESS, SIGNUP_ERROR, SIGNUP_SUCCESS, UPDATE_PROFILE_ERROR, UPDATE_PROFILE_SUCCESS } from '../../constant';

const initState = {
    authError: null
}

const authReducer = (state = initState, action) => {
    switch (action.type) {
        case LOGIN_ERROR:
            alert(action.payload.message);
            return {
                ...state,
                authError: action.payload.message,
            }
        case LOGIN_SUCCESS:
            alert("Login success");
            return {
                ...state,
                authError: null
            }
        case SIGNOUT_SUCCESS:
            return {
                ...state,
            }
        case SIGNUP_SUCCESS:
            alert("Signup success");
            return {
                ...state,
                authError: null
            }
        case SIGNUP_ERROR:
            alert(action.payload.message);
            return {
                ...state,
                authError: action.payload.message,
            }
        case UPDATE_PROFILE_SUCCESS:
            alert("Update profile success");
            return {
                ...state,
                authError: null
            }
        case UPDATE_PROFILE_ERROR:
            alert(action.payload.message);
            return {
                ...state,
                authError: action.payload.message,
            }
        default:
            return state;
    }
}

export default authReducer;
