import { ADD_TODO_SUCCESS, DELETE_ERROR, DELETE_SUCCESS, UPDATE_TODO_ERROR, UPDATE_TODO_SUCCESS } from "../../constant"

const errorState = {
    todoError: null
}

const todoReducer = (state = errorState, action) => {
    switch (action.type) {
        case ADD_TODO_SUCCESS:
            alert('Add todo success')
            return {
                ...state,
                todoError: null
            }
        case DELETE_ERROR:
            console.log(action.payload.message);
            return {
                ...state,
                todoError: action.payload.message,
            }
        case DELETE_SUCCESS:
            alert('Delete todo success')
            return {
                ...state,
                todoError: null
            }
        case UPDATE_TODO_SUCCESS:
            alert("Update todo success");
            return {
                ...state,
                todoError: null
            }
        case UPDATE_TODO_ERROR:
            alert(action.payload.message);
            return {
                ...state,
                todoError: action.payload.message,
            }
        default:
            return state
    }
}

export default todoReducer;
