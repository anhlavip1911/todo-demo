import { ADD_TODO_ERROR, ADD_TODO_SUCCESS, DELETE_ERROR, DELETE_SUCCESS, UPDATE_ERROR, UPDATE_SUCCESS, UPDATE_TODO_ERROR, UPDATE_TODO_SUCCESS } from "../../constant"

export const addTodo = (newTodo) => {
    return (dispatch, getState, { getFirebase, getFirestore }) => {

        const firestore = getFirestore();
        const profile = getState().firebase.profile;
        const authorId = getState().firebase.auth.uid;
        firestore.collection('todos').add({
            ...newTodo,
            authorFirstName: profile.firstName,
            authorLastName: profile.lastName,
            authorId: authorId,
            createdAt: new Date()
        }).then(() => {
            dispatch({
                type: ADD_TODO_SUCCESS,
                payload: newTodo
            });
        }).catch((err) => {
            dispatch({
                type: ADD_TODO_ERROR,
                payload: err
            });
        })
    }
}

export const updateTodo = (id, newTodo) => {
    return (dispatch, getState, { getFirebase, getFirestore }) => {
        const firestore = getFirestore();
        firestore.collection('todos').doc(id).update({
            ...newTodo,
            title: newTodo.title,
            content: newTodo.content,
        }).then(() => {
            dispatch({
                type: UPDATE_TODO_SUCCESS,
            });
        }).catch((err) => {
            dispatch({
                type: UPDATE_TODO_ERROR,
                payload: err
            });
        })
    }
}

export const deleteTodo = (id) => {
    return (dispatch, getState, { getFirebase, getFirestore }) => {
        const firestore = getFirestore();
        // // const profile = getState().firebase.profile;
        firestore.collection('todos').doc(id).delete({
        }).then(() => {
            dispatch({
                type: DELETE_SUCCESS,
            });
        }).catch((err) => {
            dispatch({
                type: DELETE_ERROR,
                payload: err
            });
        })
    }
}
