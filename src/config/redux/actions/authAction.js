import { LOGIN_ERROR, LOGIN_SUCCESS, SIGNUP_SUCCESS, SIGNUP_ERROR, SIGNOUT_SUCCESS, UPDATE_PROFILE_SUCCESS, UPDATE_PROFILE_ERROR } from '../../constant'

export const signInAuth = (credentials) => {
    return (dispatch, getState, { getFirebase }) => {
        const firebase = getFirebase();

        firebase.auth().signInWithEmailAndPassword(
            credentials.email,
            credentials.password
        ).then(() => {
            dispatch({
                type: LOGIN_SUCCESS,
            });
        }).catch((err) => {
            dispatch({
                type: LOGIN_ERROR,
                payload: err
            });
        })
    }
}

export const signOut = () => {
    return (dispatch, getState, { getFirebase }) => {
        const firebase = getFirebase();

        firebase.auth().signOut().then(() => {
            dispatch({
                type: SIGNOUT_SUCCESS,
            });
        }).catch((err) => {
            dispatch({
                type: LOGIN_ERROR,
                payload: err
            });
        })
    }
}

export const signUpAuth = (newUser) => {
    return (dispatch, getState, { getFirebase, getFirestore }) => {
        const firebase = getFirebase();
        const firestore = getFirestore();

        firebase.auth().createUserWithEmailAndPassword(
            newUser.email,
            newUser.password
        ).then((res) => {
            return firestore.collection('users').doc(res.user.uid).set({
                firstName: newUser.firstName,
                lastName: newUser.lastName,
                initials: newUser.lastName[0] + newUser.firstName[0],
                email: newUser.email
            })
        }).then(() => {
            dispatch({ type: SIGNUP_SUCCESS })
        }).catch((err) => {
            dispatch({ type: SIGNUP_ERROR, payload: err })
        })
    }
}

export const updateUser = (userUpdate) => {
    return (dispatch, getState, { getFirebase, getFirestore }) => {

        const firestore = getFirestore();
        // const profile = getState().firebase.profile;
        const userId = getState().firebase.auth.uid;
        firestore.collection('users').doc(userId).update({
            ...userUpdate,
            firstName: userUpdate.firstName,
            lastName: userUpdate.lastName,
            initials: userUpdate.lastName[0] + userUpdate.firstName[0],
        }).then(() => {
            dispatch({
                type: UPDATE_PROFILE_SUCCESS,
            });
        }).catch((err) => {
            dispatch({
                type: UPDATE_PROFILE_ERROR,
                payload: err
            });
        })
    }
}
