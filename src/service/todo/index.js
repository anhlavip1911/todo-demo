export const apiPost = (firestore, todo, data) => {
    firestore.collection(todo).add(data)
        .then(docRef => docRef.id)
        .catch(error => error)
}

export const apiUpdate = (firestore, todo, id, data) => {
    return firestore.collection(todo).doc(id).update(data)
}
