import React from 'react'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import SignIn from './module/auth/SignIn/'
import SignUp from './module/auth/SignUp/'
import Dashboard from './module/Dashboard'
import Navbar from './common/Components/Navbar'
import AddTodo from './module/TodoManagement/Add'
import DetailsTodo from './module/TodoManagement/Details'
import Profile from './module/ProfileManagement'
import UpdateProfile from './module/ProfileManagement/Update'
import UpdateTodo from './module/TodoManagement/Update'

export default function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <Navbar></Navbar>
        <Switch>
          <Route exact path="/" component={Dashboard} />
          <Route path="/todo/:id" component={DetailsTodo} />
          <Route path="/signin" component={SignIn} />
          <Route path="/signup" component={SignUp} />
          <Route path="/addTodo" component={AddTodo} />
          <Route path="/profile" component={Profile} />
          <Route path="/update-profile" component={UpdateProfile} />
          <Route path="/update-todo/:id" component={UpdateTodo} />
        </Switch>
      </div>
    </BrowserRouter>
  )
}
