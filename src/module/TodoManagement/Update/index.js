import React, { useEffect, useState } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types';
import { Redirect, useHistory } from 'react-router';
import { compose } from 'redux';
import { firestoreConnect } from 'react-redux-firebase';
import Loading from '../../../common/loading';
import { updateTodo } from '../../../config/redux/actions/todoAction'
import Buttonn from '../../../common/Button';
import { Link } from 'react-router-dom';

const UpdateTodo = ({ todo, auth, updateTodo, error }) => {

  const [title, setTitle] = useState()
  const [content, setContent] = useState()
  const history = useHistory()

  useEffect(() => {
    setTitle(todo?.title);
    setContent(todo?.content)
  }, [todo])

  const handleChangeTitle = (e) => {
    setTitle(e.target.value)
  }

  const handleChangeContent = (e) => {
    setContent(e.target.value);
  }

  const handleSubmit = (e) => {
    e.preventDefault();

    const newTodo = {
      title,
      content
    }

    const id = todo ? todo.id : null;
    if (window.confirm('Are you sure you wish to update?'))
      updateTodo(id, newTodo)
    history.push('/')
  }

  if (!auth.uid) return <Redirect to="/signin" />
  if (todo) {
    return (
      <div className="container">
        <form className="white" onSubmit={handleSubmit}>
          <h5 className="grey-text text-darken-3 center">Update Todo</h5>
          <div className="input-field">
            <h5 htmlFor="title">Title</h5>
            <input type="text" id='title' onChange={handleChangeTitle} value={title} />
          </div>
          <div className="input-field">
            <h5 htmlFor="content">Content</h5>
            <input type="text" id="content" onChange={handleChangeContent} value={content} />
          </div>
          <div className="input-field">
            <div className="input-field">
              <Buttonn className="btn blue lighten-1" type="submit" text="Update Todo" />
              <Link to={"/todo/" + todo.id}>
                <Buttonn className="btn pink lighten-1" type="submit" text="Cancel" />
              </Link>
            </div>
          </div>
        </form>
      </div>
    )
  } else {
    return (
      <Loading />
    )
  }
}

const mapStateToProps = (state, ownProps) => {
  const id = ownProps.match.params.id;
  const todos = state.firestore.data.todos;
  const todo = todos ? { ...todos[id], id } : null
  return {
    auth: state.firebase.auth,
    todo: todo,
    error: state.auth.authError,
  }
}

export default compose(connect(mapStateToProps, { updateTodo }),
  firestoreConnect([{
    collection: 'todos'
  }])
)(UpdateTodo);
