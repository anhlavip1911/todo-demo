import React, { useState } from 'react'
import { connect } from 'react-redux'
import { addTodo } from '../../../config/redux/actions/todoAction'
import PropTypes from 'prop-types';
import { Redirect, useHistory } from 'react-router';
import Input from '../../../common/Input';
import Buttonn from '../../../common/Button';
import { Link } from 'react-router-dom';

const AddTodo = ({ addTodo, auth }) => {

    const [title, setTitle] = useState('')
    const [content, setContent] = useState('')
    const history = useHistory()

    const handleChangeTitle = (e) => {
        setTitle(e.target.value)
    }

    const handleChangeContent = (e) => {
        setContent(e.target.value);
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        const newTodo = {
            title,
            content
        }
        addTodo(newTodo)
        history.push('/')
    }

    if (!auth.uid) return <Redirect to="/signin" />
    return (
        <div className="container">
            <form className="white" onSubmit={handleSubmit}>
                <h5 className="grey-text text-darken-3">Create a New Todo</h5>
                <div className="input-field">
                    <Input text="Todo Title" type="text" id='title' onChange={handleChangeTitle} />
                </div>
                <div className="input-field">
                    <Input text="Todo Content" type="text" id="content" onChange={handleChangeContent} />
                </div>
                <div className="input-field">
                    <Buttonn className="btn blue lighten-1" type="submit" text="Create" />
                    <Link to="/">
                        <Buttonn className="btn pink lighten-1" type="submit" text="Cancel" />
                    </Link>
                </div>
            </form>
        </div>
    )
}

AddTodo.propTypes = {
    addTodo: PropTypes.func.isRequired
}

const mapStateToProps = (state, ownProps) => {
    return {
        auth: state.firebase.auth
    }
}

export default connect(mapStateToProps, { addTodo })(AddTodo)
