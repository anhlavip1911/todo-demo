import React from 'react'
import { connect } from 'react-redux';
import { firestoreConnect } from 'react-redux-firebase';
import { compose } from 'redux';
import { Link, Redirect, useHistory } from 'react-router-dom';
import moment from 'moment';
import Loading from '../../../common/loading'
import { deleteTodo } from '../../../config/redux/actions/todoAction'
import Buttonn from '../../../common/Button';

const DetailsTodo = ({ todo, auth, deleteTodo }) => {
    const history = useHistory()
    if (!auth.uid) return <Redirect to="/signin" />

    const handleSubmit = (e) => {
        e.preventDefault();
        const id = todo ? todo.id : null;
        if (window.confirm('Are you sure you wish to delete this item?'))
            deleteTodo(id)
        history.push('/')
    }

    if (todo) {
        console.log(todo.id);
        return (
            <div className="container section project-details">
                <div className="card z-depth-0">
                    <div className="card-content">
                        <span className="card-title">{todo.title}</span>
                        <p>{todo.content}</p>
                    </div>
                    <div className="card-action grey lighten-4 grey-text">
                        <div>Posted by {todo.authorFirstName} {todo.authorLastName}</div>
                        <div>{todo.createdAt && moment(todo.createdAt.toDate()).calendar()}</div>
                    </div>
                </div>
                <Link to={"/update-todo/" + todo.id}>
                    <Buttonn className="btn blue lighten-1" type="submit" text="Update" />
                </Link>
                <Buttonn
                    onClick={handleSubmit}
                    className="btn pink lighten-1" type="submit" text="Delete" />
            </div>
        )
    } else {
        return (
            <Loading />
        )
    }
}

const mapStateToProps = (state, ownProps) => {
    const id = ownProps.match.params.id;
    // console.log(state);
    const todos = state.firestore.data.todos;
    const todo = todos ? { ...todos[id], id } : null
    return {
        todo: todo,
        auth: state.firebase.auth,
    }
}

export default compose(connect(mapStateToProps, { deleteTodo }),
    firestoreConnect([{
        collection: 'todos'
    }])
)(DetailsTodo);
