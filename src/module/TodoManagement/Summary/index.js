import moment from 'moment'
import React from 'react'

const TodoSummary = ({ todos }) => {
    return (
        <div className="card z-depth-0 todo-summary">
            <div className="card-content grey-text text-darken-3">
                <span className="card-title">{todos.title}</span>
                <p>Posted by the {todos.authorFirstName} {todos.authorLastName}</p>
                <p className='grey-text'>
                    {todos.createdAt && moment(todos.createdAt.toDate()).calendar()}
                </p>
            </div>
        </div>
    )
}

export default TodoSummary
