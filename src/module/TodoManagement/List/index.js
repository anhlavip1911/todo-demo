import React from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import TodoSummary from '../Summary'

const ListTodo = ({ listTodos }) => {
    console.log(listTodos);
    return (
        <div className="todo-list section">
            {listTodos && listTodos.map(todo => {
                return (
                    <div >
                        <Link to={"/todo/" + todo.id} key={todo.id} >
                            <TodoSummary todos={todo} />
                        </Link>
                    </div>
                )
            })}
        </div>
    )
}

const mapStateToProps = (state, ownProps) => {
    return {
        listTodos: state.firestore.ordered.todos,
        auth: state.firebase.auth
    }
}

export default connect(mapStateToProps, null)(ListTodo)
