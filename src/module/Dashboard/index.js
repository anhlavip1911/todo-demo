import React from 'react'
import { connect } from 'react-redux'
import ListTodo from '../TodoManagement/List'
import PropTypes from 'prop-types';
import { firestoreConnect } from 'react-redux-firebase'
import { compose } from 'redux'
import { Redirect } from 'react-router';
import Loading from '../../common/loading';

const Dashboard = ({ todos, auth }) => {

    if (!auth.uid) return <Redirect to="/signin" />
    if (todos) {
        return (
            <div className="dashboard container">
                <div className="row">
                    <div className="col s12 m6">
                        <ListTodo />
                    </div>
                </div>
            </div>
        )
    } else {
        return (
            <Loading />
        )
    }

}

const mapStateToProps = (state, ownProps) => {
    return {
        todos: state.firestore.data.todos,
        auth: state.firebase.auth
    }
}

export default compose(connect(mapStateToProps),
    firestoreConnect([{
        collection: 'todos'
    }])
)(Dashboard);