import React, { useState } from 'react'
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Redirect, useHistory } from 'react-router';
import { signUpAuth } from '../../../config/redux/actions/authAction'
import Input from '../../../common/Input';
import Buttonn from '../../../common/Button';
import Loading from '../../../common/loading';

const SignUp = ({ auth, signUpAuth, error }) => {

    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [firstName, setFirstName] = useState('')
    const [lastName, setLastName] = useState('')
    const history = useHistory();

    const handleChangeEmail = (e) => {
        setEmail(e.target.value)
    }

    const handleChangePw = (e) => {
        setPassword(e.target.value);
    }

    const handleChangeFirstName = (e) => {
        setFirstName(e.target.value);
    }

    const handleChangeLastName = (e) => {
        setLastName(e.target.value);
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        const newUser = {
            email,
            password,
            firstName,
            lastName
        }
        signUpAuth(newUser);
        console.log(error);
    }

    if (auth.uid) { return <Redirect to="/profile" /> }

    return (
        <div className="container">
            <form className="white" onSubmit={handleSubmit}>
                <h5 className="grey-text text-darken-3">Sign Up</h5>
                <div className="input-field">
                    <Input text="Email" type="email" id='email' onChange={handleChangeEmail} />
                </div>
                <div className="input-field">
                    <Input text="Password" type="password" id='password' onChange={handleChangePw} />
                </div>
                <div className="input-field">
                    <Input text="First Name" type="text" id='firstName' onChange={handleChangeFirstName} />
                </div>
                <div className="input-field">
                    <Input text="Last Name" type="text" id='lastName' onChange={handleChangeLastName} />
                </div>
                <div className="input-field">
                    <Buttonn disabled={false} className="btn blue lighten-1" type="submit" text="Sign Up" />
                </div>
            </form>
        </div>
    )
}

SignUp.propTypes = {
    signUpAuth: PropTypes.func.isRequired
}

const mapStateToProps = (state, ownProps) => {
    return {
        auth: state.firebase.auth,
        error: state.auth.authError,
    }
}

export default connect(mapStateToProps, { signUpAuth })(SignUp)
