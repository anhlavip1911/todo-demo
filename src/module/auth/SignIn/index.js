import React, { useState } from 'react'
import { connect } from 'react-redux';
import { Redirect } from 'react-router';
import Buttonn from '../../../common/Button';
import Input from '../../../common/Input';
import { signInAuth } from '../../../config/redux/actions/authAction'

const SignIn = ({ error, signInAuth, auth }) => {

    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')

    const handleChangeEmail = (e) => {
        setEmail(e.target.value)
    }

    const handleChangePw = (e) => {
        setPassword(e.target.value);
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        const account = {
            email,
            password
        }
        signInAuth(account)
    }

    if (auth.uid) return <Redirect to="/" />

    return (
        <div className="container">
            <form className="white" onSubmit={handleSubmit}>
                <h5 className="grey-text text-darken-3">Sign In</h5>
                <div className="input-field">
                    <Input htmlFor="email" text="Email" type="email" id='email' onChange={handleChangeEmail} />
                </div>
                <div className="input-field">
                    <Input htmlFor="password" text="Password" type="password" id='password' onChange={handleChangePw} />
                </div>
                <div className="input-field">
                    <Buttonn className="btn blue lighten-1" type="submit" text="Login" />
                </div>
            </form>
        </div>
    )
}

const mapStateToProps = (state, ownProps) => {
    return {
        error: state.auth.authError,
        auth: state.firebase.auth
    }
}

export default connect(mapStateToProps, { signInAuth })(SignIn)
