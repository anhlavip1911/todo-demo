import React, { useEffect, useState } from 'react'
import { connect } from 'react-redux';
import { Link, Redirect, useHistory } from 'react-router-dom';
import Buttonn from '../../../common/Button';
import Loading from '../../../common/loading';
import { updateUser } from '../../../config/redux/actions/authAction'

const UpdateProfile = ({ auth, profile, updateUser }) => {
  const history = useHistory()
  const [firstName, setFirstName] = useState('')
  const [lastName, setLastName] = useState('')

  useEffect(() => {
    setFirstName(profile?.firstName);
    setLastName(profile?.lastName)
  }, [profile])

  const handleChangeFirstName = (e) => {
    setFirstName(e.target.value);
  }

  const handleChangeLastName = (e) => {
    setLastName(e.target.value);
  }

  const handleSubmit = (e) => {
    e.preventDefault();
    const update = {
      firstName,
      lastName
    }
    if (window.confirm('Are you sure you wish to update?')) 
    updateUser(update);
    history.push('/profile')
  }

  if (!auth.uid) return <Redirect to="/" />
  console.log(profile);
  // console.log(auth);
  if (profile.isLoaded) {
    return (
      <div className="container">
        <form className="white" onSubmit={handleSubmit}>
          <h5 className="grey-text text-darken-3 center">Update Profile</h5>
          <div className="input-field">
            <h5 htmlFor="name">First Name</h5>
            <input type="text" id='firstName' value={firstName} onChange={handleChangeFirstName} />
          </div>
          <div className="input-field">
            <h5 htmlFor="name">Last Name</h5>
            <input type="text" id='lastName' value={lastName} onChange={handleChangeLastName} />
          </div>
          <div className="input-field">
            <Buttonn className="btn blue lighten-1" type="submit" text="Update" />
            <Link to={"/profile"}>
              <Buttonn className="btn pink lighten-1" type="submit" text="Cancel" />
            </Link>
          </div>
        </form>
      </div>
    )
  } else {
    return <Loading />
  }
}

const mapStateToProps = (state, ownProps) => {
  console.log(state);

  return {
    auth: state.firebase.auth,
    profile: state.firebase.profile,
    error: state.auth.authError,
  }
}

export default connect(mapStateToProps, { updateUser })(UpdateProfile)
