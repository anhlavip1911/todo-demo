import React from 'react'
import { connect } from 'react-redux';
import { Link, Redirect } from 'react-router-dom';
import Buttonn from '../../common/Button';
import Input from '../../common/Input';
import Loading from '../../common/loading';

const Profile = ({ auth, profile }) => {

    if (!auth.uid) return <Redirect to="/" />
    // console.log(profile);
    if (profile.isLoaded) {
        return (
            <div className="container">
                <form className="white">
                    <h5 className="grey-text text-darken-3 center">My Profile</h5>
                    <div className="input-field">
                        <h5 htmlFor="email">Email</h5>
                        <Input type="email" id='email' value={auth.email} />
                    </div>
                    <div className="input-field">
                        <h5 htmlFor="name">First Name</h5>
                        <Input type="text" id='firstName' value={profile.firstName} />
                    </div>
                    <div className="input-field">
                        <h5 htmlFor="name">Last Name</h5>
                        <Input type="text" id='lastName' value={profile.lastName} />
                    </div>
                    <div className="input-field center">
                        <Link to="/update-profile">
                            <Buttonn className="btn blue lighten-1" type="submit" text="Go to Update Profile" />
                        </Link>
                    </div>
                </form>
            </div>
        )
    } else {
        return <Loading />
    }
}

const mapStateToProps = (state, ownProps) => {
    console.log(state);
    return {
        auth: state.firebase.auth,
        profile: state.firebase.profile
    }
}

export default connect(mapStateToProps)(Profile)
