import React from 'react'

function Buttonn(props) {
    return (
        <>
            <button {...props}> {props.text}</button>
        </>
    )
}

export default Buttonn
