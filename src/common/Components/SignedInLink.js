import React from 'react'
import { connect } from 'react-redux'
import { NavLink } from 'react-router-dom'
import { signOut } from '../../config/redux/actions/authAction'

const SignedInLink = ({ signOut, profile }) => {
  const handleClick = (e) => {
    if (window.confirm('Are you sure you wish to log out?'))
      signOut(e)
  }

  return (
    <div>
      <ul className="right">
        <li><NavLink to='/addTodo'>New Todo</NavLink></li>
        <li><a onClick={handleClick}>Log Out</a></li>
        <li><NavLink to='/profile' className="btn btn-floating pink lighten-1">{profile.initials}</NavLink></li>
      </ul>
    </div>
  )
}

const mapStateToProps = (state, ownProps) => {
  return {
    profile: state.firebase.profile
  }
}

export default connect(mapStateToProps, { signOut })(SignedInLink)
