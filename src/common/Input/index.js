import React from 'react'

function Input(props) {
    return (
        <>
            <label {...props}>{props.text}</label>
            <input {...props} />
        </>
    )
}

export default Input
